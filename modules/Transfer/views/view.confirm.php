<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

require_once('modules/Transfer/views/TransferView.php');

class TransferViewConfirm extends TransferView {

    public function display() {
        $source = !empty($_REQUEST['source']) ? $_REQUEST['source'] : null;
        $target = !empty($_REQUEST['target']) ? $_REQUEST['target'] : null;
        $limit = !empty($_REQUEST['limit']) ? $_REQUEST['limit'] : null;

        $sourceBean = loadBean($source);

        if($limit == 'all'){
            $sources = $sourceBean->get_full_list();
        } else {
            $sourceObj = $sourceBean->get_list('', '', 0, $limit);
            $sources = $sourceObj['list'];
        }

        $newIds = $dupeIds = array();
        foreach ($sources as $key => $rows) {
            $targetBean = loadBean($target);
            $targetBean->populateDefaultValues();

            foreach ($rows as $sourceCol => $sourceVal) {
                if (is_array($sourceVal) || $sourceCol == 'id')
                    continue;

                if (array_key_exists('source_col_' . $sourceCol, $_REQUEST) && !empty($sourceVal)) {
                    $targetCol = $_REQUEST['source_col_' . $sourceCol];
                    $targetBean->$targetCol = $sourceVal;
                }
            }

            $where = array();
            foreach ($_REQUEST['dupecheck'] as $targetColCheck) {
                if (!empty($targetBean->$targetColCheck)) {
                    $where[] = "$target.$targetColCheck='{$targetBean->$targetColCheck}'";
                }
            }

            $whereStr = implode(' OR ', $where);

            $targetRow = $targetBean->get_list('', $whereStr, 0, 1);

            if (empty($targetRow['list'])) {
                $targetBean->beforeImportSave();
                $newIds[] = $targetBean->save(false);
                $targetBean->afterImportSave();
            } else {
                $dupeIds[] = $targetRow['list'][0]->id;
            }
        }

        $this->ss->assign('pageTitle', $this->_getLabel('LBL_STEP_4_TITLE'));
        $this->ss->assign('source', $source);
        $this->ss->assign('target', $target);
        $this->ss->assign('newIds', $newIds);
        $this->ss->assign('dupeIds', $dupeIds);
        $this->render();
    }
}
