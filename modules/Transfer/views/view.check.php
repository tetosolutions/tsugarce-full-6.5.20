<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

require_once('modules/Transfer/views/TransferView.php');

class TransferViewCheck extends TransferView {

    public function display() {
        $target = !empty($_REQUEST['target']) ? $_REQUEST['target'] : null;
        $limit = !empty($_REQUEST['limit']) ? $_REQUEST['limit'] : null;

        $targetBean = loadBean($target);

        $indexes = $targetBean->getIndices();

        if ($targetBean->hasCustomFields()) {
            $custmIndexes = $targetBean->db->helper->get_indices($targetBean->table_name . '_cstm');
            $indexes = array_merge($custmIndexes, $indexes);
        }

        require_once('include/export_utils.php');

        $import_fields = $targetBean->get_importable_fields();
        $importable_keys = array_keys($import_fields); //

        $mstr_exclude_array = array('all' => array('team_set_id', 'id', 'deleted'), 'contacts' => array('email2'), array('leads' => 'reports_to_id'), array('prospects' => 'tracker_key'));

        $exclude_array = isset($mstr_exclude_array[strtolower($targetBean->module_dir)]) ? array_merge($mstr_exclude_array[strtolower($targetBean->module_dir)], $mstr_exclude_array['all']) : $mstr_exclude_array['all'];

        $indexArray = $fieldsUsed = array();
        foreach ($indexes as $index) {
            if ($index['type'] == "index") {
                $labelsArray = array();
                foreach ($index['fields'] as $field) {
                    $fieldName = '';

                    if (!in_array($field, $importable_keys) || in_array($field, $exclude_array))
                        continue;
                    $fieldDef = $targetBean->getFieldDefinition($field);

                    if (in_array($fieldDef['name'], $fieldsUsed))
                        continue;

                    $fieldName = translateForExport($fieldDef['name'], $targetBean);

                    $indexArray[$fieldDef['name']] = $fieldName;
                    $fieldsUsed[] = $fieldDef['name'];
                }
            }
        }

        if (in_array('first_name', $fieldsUsed) && in_array('last_name', $fieldsUsed)) {
            $indexArray['full_name'] = translateForExport('full_name', $targetBean);
            $fieldsUsed[] = 'full_name';
        }

        asort($indexArray);

        $dupes = array();
        foreach ($indexArray as $ik => $iv) {
            $dupes[] = array("dupeVal" => $ik, "label" => $iv);
        }

        $this->ss->assign('pageTitle', $this->_getLabel('LBL_STEP_3_TITLE'));
        $this->ss->assign('dupes', $dupes);
        $this->ss->assign('limit', $limit);
        $this->render();
    }

}
