<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

require_once('modules/Transfer/views/TransferView.php');

class TransferViewList extends TransferView {

    public function display() {
        global $app_list_strings;

        $source = !empty($_REQUEST['source']) ? $_REQUEST['source'] : null;

        $this->ss->assign('pageTitle', $this->_getLabel('LBL_STEP_1_TITLE'));
        $this->ss->assign('source', $source);
        $this->ss->assign('options', $app_list_strings['moduleList']);

        $this->render();
    }

}
