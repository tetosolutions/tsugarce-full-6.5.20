<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

class TransferView extends SugarView {

    public function render() {
        $content = $this->ss->fetch("modules/Transfer/tpls/{$this->action}.tpl");
        $this->ss->assign('CONTENT', $content);
        $this->ss->display('modules/Transfer/tpls/layout.tpl');
    }

    function _getLabel($label) {
        global $mod_strings;

        return $mod_strings[$label];
    }

}
