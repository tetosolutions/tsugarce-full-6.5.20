<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

require_once('modules/Transfer/views/TransferView.php');

class TransferViewMatch extends TransferView {

    public function display() {
        global $current_language;

        $source = !empty($_REQUEST['source']) ? $_REQUEST['source'] : null;
        $target = !empty($_REQUEST['target']) ? $_REQUEST['target'] : null;
        $limit = !empty($_REQUEST['limit']) ? $_REQUEST['limit'] : null;

        $sourceBean = loadBean($source);
        $targetBean = loadBean($target);

        $sourceObj = $sourceBean->get_list('', '', null, 1);
        $sourceRow = $sourceObj['list'][0]->get_list_view_data();

        $sourceFields = $sourceBean->get_importable_fields();
        $targetFields = $targetBean->get_importable_fields();

        $sourceModStrings = return_module_language($current_language, $sourceBean->module_dir);
        $targetModStrings = return_module_language($current_language, $targetBean->module_dir);

        $rows = array();
        foreach ($sourceFields as $sourceFieldName => $sourceFieldProps) {
            $sourceDisplayName = str_replace(":", "", translate($sourceFieldProps['name'], $sourceBean->module_dir));
            if (!empty($sourceModStrings['LBL_EXPORT_' . strtoupper($sourceFieldName)])) {
                $sourceDisplayName = str_replace(":", "", $sourceModStrings['LBL_EXPORT_' . strtoupper($sourceFieldName)]);
            } else if (!empty($sourceFieldProps['vname'])) {
                $sourceDisplayName = str_replace(":", "", translate($sourceFieldProps['vname'], $sourceBean->module_dir));
            }

            $options = array();
            foreach ($targetFields as $targetFieldName => $targetFieldProps) {
                $displayname = str_replace(":", "", translate($targetFieldProps['name'], $targetBean->module_dir));
                if (!empty($targetModStrings['LBL_EXPORT_' . strtoupper($targetFieldName)])) {
                    $displayname = str_replace(":", "", $targetModStrings['LBL_EXPORT_' . strtoupper($targetFieldName)]);
                } else if (!empty($targetFieldProps['vname'])) {
                    $displayname = str_replace(":", "", translate($targetFieldProps['vname'], $targetBean->module_dir));
                }
                $selected = $targetFieldName == $sourceFieldName ? 'selected' : '';
                $options[$displayname] = "<option value='$targetFieldName' $selected>$displayname</option>";
            }

            ksort($options);

            $cellTwoData = $sourceRow[strtoupper($sourceFieldName)];

            $rows[$sourceDisplayName] = array(
                'field_choices' => implode('', $options),
                'cell1' => $sourceDisplayName,
                'cell2' => strip_tags($cellTwoData),
                'field' => $sourceFieldName,
            );
        }

        ksort($rows);

        $this->ss->assign('pageTitle', $this->_getLabel('LBL_STEP_2_TITLE'));
        $this->ss->assign('source', $source);
        $this->ss->assign('target', $target);
        $this->ss->assign('limit', $limit);
        $this->ss->assign('rows', $rows);

        $this->render();
    }

}
