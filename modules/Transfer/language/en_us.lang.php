<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

$mod_strings = array(
    'LBL_NEXT' => 'Next >',
    'LBL_TRANSFER' => 'Confirm Transfer',
    'LBL_HEADER_ROW' => 'Source Module Columns',
    'LBL_HEADER_ROW_HELP' => 'This column displays the labels in the header row of the import file.',
    'LBL_DATABASE_FIELD' => 'Target Module Columns',
    'LBL_DATABASE_FIELD_HELP' => 'This column displays all of the fields in the module. Select a field to map to the data in the import file rows.',
    'LBL_ROW' => 'Source Sample Record',
    'LBL_ROW_HELP' => 'This column displays the data in the first non-header row of the import file. If the header row labels are appearing in this column, click Back to specify the header row in the Import File Properties.',
    'LBL_DONT_MAP' => '-- Do not map this field --',
    'LBL_STEP_1_TITLE' => 'Step 1: Select Source / Target Modules',
    'LBL_STEP_2_TITLE' => 'Step 2: Confirm Field Mappings',
    'LBL_STEP_3_TITLE' => 'Step 3: Check for Possible Duplicates',
    'LBL_STEP_4_TITLE' => 'Step 4: View Import Results',
    'LBL_SELECT' => '-- Select --',
    'LBL_TRANSFER_LIMIT_HELP' => 'Use this to limit the number of records to be transferred. Default value is 30.',
    'LBL_SELECT_DUPLICATE_INSTRUCTION' => 'To avoid creating duplicate records, select which of the mapped fields you would like to use to perform a duplicate check while data is being imported. Values within existing records in the selected fields will be checked against the data in the import file. If matching data is found, the rows in the import file containing the data will be displayed along with the import results (next page). You will then be able to select which of these rows to continue importing.',
    'LBL_SELECT_DUPLICATE_FIELDS' => 'Select Fields for Duplicate Check',
);