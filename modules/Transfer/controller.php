<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

class TransferController extends SugarController {

    public function action_select() {
        $this->view = 'select';
    }

    public function action_match() {
        $this->view = 'match';
    }

    public function action_check() {
        $this->view = 'check';
    }

    public function action_confirm() {
        $this->view = 'confirm';
    }

    public function action_listview() {
        $this->view = 'list';
    }

}
