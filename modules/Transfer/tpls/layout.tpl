<style>
{literal}
.container {font-size:12px;}
.moduleTitle {padding-bottom: 3px;padding-top: 0;margin-bottom: 10px;margin-top: 0;}
.moduleTitle h2{font-size: 18px;}
{/literal}
</style>
<div class="container">
    <div class="moduleTitle">
        <h2>{$pageTitle}</h2>
    </div>
    {$CONTENT}
</div>