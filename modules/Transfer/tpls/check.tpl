{$MOD.LBL_SELECT_DUPLICATE_INSTRUCTION}
<br/><br/>
{$MOD.LBL_SELECT_DUPLICATE_FIELDS}:
<br/><br/>
<form enctype="multipart/form-data" id="transfer" name="transfer" method="POST" action="index.php">
    <input type="hidden" name="module" value="Transfer">
    <input type="hidden" name="action" value="confirm">

    {foreach from=$smarty.request key=k item=v}
        {if $k neq 'current_step'}
            {if is_array($v)}
                {foreach from=$v key=k1 item=v1}
                    <input type="hidden" name="{$k}[]" value="{$v1}">
                {/foreach}
            {else}
                {if $k != 'action' && $k != 'module' && $k != 'button'}
                <input type="hidden" name="{$k}" value="{$v}">
                {/if}
            {/if}
        {/if}
    {/foreach}

    <select class='fixedwidth' name="dupecheck[]" multiple="yes" size="15">
    {foreach from=$dupes key=key item=item name=rows}
        <option value="{$item.dupeVal}">{$item.label}</option>
    {/foreach}
    </select>
    <br /><br /><br />

    <table width="100%" cellpadding="2" cellspacing="0" border="0">
    <tr>
        <td align="left">
            <input title="{$MOD.LBL_NEXT}"  class="button" type="submit" name="button" value="  {$MOD.LBL_TRANSFER}  " id="gonext">
        </td>
    </tr>
    </table>
</form>