
<form enctype="multipart/form-data" name="transfer" method="post" action="index.php" id="transfer">
    <input type="hidden" name="module" value="Transfer">
    <input type="hidden" name="action" value="match">

    Source Module : 
    {if $source === null}
    <select name="source">
        <option value="-1">{$MOD.LBL_SELECT}</option>
        {foreach from=$options key=key item=item}
            <option value="{$key}">{$item}</option>
        {/foreach}
    </select>
    {else}
    <input type="hidden" name="source" value="{$source}">
    <b>{$source}</b>
    {/if}
    <br/>
    Target Module : 
    <select name="target">
        <option value="-1">{$MOD.LBL_SELECT}</option>
        {foreach from=$options key=key item=item}
            <option value="{$key}">{$item}</option>
        {/foreach}
    </select>
    <br/>
    Target Limit : 
    <input type="text" name="limit" /> {sugar_help text=$MOD.LBL_TRANSFER_LIMIT_HELP}
    <br/>
    <input title="{$MOD.LBL_NEXT}"  class="button" type="submit" name="button" value="  {$MOD.LBL_NEXT}  " id="gonext">
</form>
