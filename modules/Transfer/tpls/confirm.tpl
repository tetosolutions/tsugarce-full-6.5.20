<br/><br/>
<b>New {$source}</b> Created :<br/><br/>
{foreach from=$newIds item=item}
    <a href="index.php?module={$source}&action=EditView&record={$item}" target="_blank">{$item}</a><br/>
{/foreach}

<br/><br/>
<b>Skipped {$source} (duplicate)</b> :<br/><br/>
{foreach from=$dupeIds item=item}
    <a href="index.php?module={$target}&action=EditView&record={$item}" target="_blank">{$item}</a><br/>
{/foreach}
<br/><br/>