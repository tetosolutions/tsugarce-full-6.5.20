<form enctype="multipart/form-data" name="transfer" method="post" action="index.php" id="transfer">
    <input type="hidden" name="module" value="Transfer">
    <input type="hidden" name="action" value="match">

    Source Module : 
    {if $source === null}
    <select name="source">
        <option value="-1">{$MOD.LBL_SELECT}</option>
        {foreach from=$options key=key item=item}
            <option value="{$key}">{$item}</option>
        {/foreach}
    </select>
    {else}
    <input type="hidden" name="source" value="{$source}">
    <b>{$source}</b>
    {/if}
    <br/>
    Target Module : 
    <select name="target">
        <option value="-1">{$MOD.LBL_SELECT}</option>
        {foreach from=$options key=key item=item}
            <option value="{$key}">{$item}</option>
        {/foreach}
    </select>
    <br/>
    Target Limit : 
    <select name="limit">
        <option value="-1">{$MOD.LBL_SELECT}</option>
        <option value="0">All</option>
        <option value="30">30</option>
        <option value="60">60</option>
    </select>
    <br/>
    <input title="{$MOD.LBL_NEXT}"  class="button" type="submit" name="button" value="  {$MOD.LBL_NEXT}  " id="gonext">
</form>
