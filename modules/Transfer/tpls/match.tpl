
<form enctype="multipart/form-data" id="transfer" name="transfer" method="POST" action="index.php">
    <input type="hidden" name="module" value="Transfer">
    <input type="hidden" name="action" value="check">
    <input type="hidden" name="source" value="{$source}">
    <input type="hidden" name="target" value="{$target}">
    <input type="hidden" name="limit" value="{$limit}">

    <table border="1" cellspacing="1" cellpadding="1" width="100%" id="importTable" class="detail view">
    {foreach from=$rows key=key item=item name=rows}
        {if $smarty.foreach.rows.first}
            <tr>
                <td><b>{$MOD.LBL_HEADER_ROW}</b> {sugar_help text=$MOD.LBL_HEADER_ROW_HELP}</td>
                <td><b>{$MOD.LBL_DATABASE_FIELD}</b> {sugar_help text=$MOD.LBL_DATABASE_FIELD_HELP}</td>
                <td><b>{$MOD.LBL_ROW}</b> {sugar_help text=$MOD.LBL_ROW_HELP}</td>
            </tr>
        {/if}
        <tr>
            <td>{$item.cell1}</td>
            <td>
                <select class='fixedwidth' name="source_col_{$item.field}">
                        <option value="-1">{$MOD.LBL_DONT_MAP}</option>
                        {$item.field_choices}
                </select>
            </td>
            <td>{$item.cell2}</td>
        </tr>
    {/foreach}
    </table>

    <br />

    <table width="100%" cellpadding="2" cellspacing="0" border="0">
    <tr>
        <td align="left">
            <input title="{$MOD.LBL_NEXT}"  class="button" type="submit" name="button" value="  {$MOD.LBL_NEXT}  " id="gonext">
        </td>
    </tr>
    </table>
</form>